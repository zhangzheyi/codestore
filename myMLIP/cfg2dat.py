# -*- coding:utf-8 -*-
'''
This code is used to convert MLIP's cfg file to lammps-config/vasp-poscar
Thus enable these structures to be visualized easily by other software e.g.OVITO
Multiple options were supported:
   python3 cfg2dat.py -h
   python3 cfg2dat.py -i cfgfile -o outfile(this4config)
   python3 cfg2dat.py -i cfgfile -t vasp
@ Author: Zheyi Zhang (DUT, China)
@ Email : zhangzheyi@mail.dlut.edu.cn or zhangzy3033@163.com
'''
import os,time,shutil
import sys,getopt
import numpy as np

def readargv(strs):
    workpath  = os.getcwd()
    runtype   = 'lammps'
    inputfile = 'training_dataset_300K.cfg'
    outfile   = inputfile.rsplit('.',1)[0]+'.config'
    optsinfo  = 'Options:\n'+ \
                '  -h, --help              Show option choice\n'+\
                '  -i file, --input file   Must provide a filename for input when using this option\n'+\
                '  -t type, --type type    Must set to be a certain type, it only supports lammps/vasp\n'+\
                '  -o file, --output file  Must provide a filename for output when using this option\n'+\
                '  -p fpath, --path fpath  Must provide a workpath for input(be the same as output)\n'
    if strs:
        try:
            opts,args = getopt.getopt(strs,'ht:i:o:p:',['help','type=','input=','output=','path='])
        except getopt.GetoptError as e:
            print('Wrong Options: %s'%(str(e)),flush=True)
            print('Correct '+optsinfo,flush=True)
            exit()
        if args:
            print('Wrong Options: %s not recognized'%(str(args).split('[')[-1].split(']')[0]),flush=True)
            print('Correct '+optsinfo,flush=True)
            exit()
        for opt,arg in opts:
            if opt == '-h' or opt == '--help':
                print(optsinfo,flush=True)
                exit()
            if opt == '-i' or opt == '--input':
                inputfile = arg
                outfile   = inputfile.rsplit('.',1)[0]+'.config'
            if opt == '-o' or opt == '--output':
                outfile   = arg
            if opt == '-t' or opt == '--type':
                runtype   = arg.lower()
                if runtype not in ['lammps','vasp']:
                    print(optsinfo,flush=True)
                    exit()
            if opt == '-p' or opt == '--path':
                workpath = arg
    else:
        print('\nRunning program without any inputs',flush=True)
    return workpath,inputfile,outfile,runtype

def lattice2lammps(vector):
    lammpslattice = np.zeros((3,3))
    Ahat    = vector[0]/np.sqrt(np.sum(np.square(vector[0])))
    AcrossB = np.cross(vector[0],vector[1])
    AcrossBh= AcrossB/np.sqrt(np.sum(np.square(AcrossB)))
    lammpslattice[0,0] = np.sqrt(np.sum(np.square(vector[0])))
    lammpslattice[1,1] = np.sqrt(np.sum(np.square(np.cross(Ahat,vector[1]))))
    lammpslattice[2,2] = np.sqrt(np.sum(np.square(vector[2].dot(AcrossBh))))
    lammpslattice[1,0] = vector[1].dot(Ahat) # xy
    lammpslattice[2,0] = vector[2].dot(Ahat) # xz
    lammpslattice[2,1] = vector[2].dot(np.cross(AcrossBh,Ahat)) # yz
    return lammpslattice

def cfg2vasp(workpath,filename):
    folder = 'poscars'
    while True:
        if folder in os.listdir(workpath):
            if os.path.isfile(os.path.join(workpath,folder)):
                folder += 's'
            else:
                shutil.rmtree(os.path.join(workpath,folder))
                os.mkdir(os.path.join(workpath,folder))
                break
        else:
            os.mkdir(os.path.join(workpath,folder))
            break
    with open(os.path.join(workpath,filename),'r+') as fr:
        frames=0
        while True:
            line = fr.readline()
            if not line:
                break
            if not line.split():
                continue
            if 'BEGIN_CFG' in line:
                lattice_frame = np.zeros((3,3))
                for i in range(7):
                    line = fr.readline()
                    if i == 1:
                        atoms = int(line.split()[0])
                        atoms_frame   = np.zeros((atoms,5))
                    if i in [3,4,5]:
                        data = line.split()
                        lattice_frame[i-3,:] = np.array(list(map(float,data)))        
                for i in range(atoms):
                    data = fr.readline().split()
                    atoms_frame[i,:] =  np.array(list(map(float,data[0:5])))
                while True:
                    line = fr.readline()
                    if 'END_CFG' in line:
                        break
                frames += 1
                tmppath = os.path.join(workpath,folder)
                with open(os.path.join(tmppath,'POSCAR'+str(frames)),'w') as fw:
                    fw.write('System = unknown %d atoms\n'%(atoms))
                    fw.write('  1.0\n')
                    for i in range(3):
                        fw.write('  %.8f  %.8f  %.8f\n'%(lattice_frame[i,0],lattice_frame[i,1],lattice_frame[i,2]))
                    types = np.amax(atoms_frame[:,1])+1
                    for i in range(int(types)):
                        fw.write('  Type_%d'%(i+1))
                    fw.write('\n')
                    for i in range(int(types)):
                        fw.write('   %d'%(int(np.sum(atoms_frame[:,1] == i))))
                    fw.write('\nDirect\n')
                    atoms_frame_sort = atoms_frame[np.argsort(atoms_frame[:,1])]
                    atoms_frame_dire = atoms_frame_sort[:,2:].dot(np.array(np.matrix(lattice_frame).I))
                    for i in range(atoms):
                        fw.write('  %.8f  %.8f  %.8f\n'%(atoms_frame_dire[i,0],atoms_frame_dire[i,1],atoms_frame_dire[i,2]))
    print("\n%d frames in MLIP's cfg file has been converted to vasp poscar files...\n"%(frames),flush=True)               
                  
def cfg2config(workpath,filename,dumpname,keepforce=False):
    if os.path.exists(os.path.join(workpath,dumpname)):
        os.remove(os.path.join(workpath,dumpname))
    with open(os.path.join(workpath,filename),'r+') as fr:
        frames = 0
        while True:
            line = fr.readline()
            if not line:
                break
            if not line.split():
                continue
            if 'BEGIN_CFG' in line:
                lattice_frame = np.zeros((3,3))
                for i in range(7):
                    line = fr.readline()
                    if i == 1:
                        atoms = int(line.split()[0])
                    if i in [3,4,5]:
                        data = line.split()
                        lattice_frame[i-3,:] = np.array(list(map(float,data)))        
                    if i == 6:
                        datalen = len(line.split())-1
                        atoms_frame   = np.zeros((atoms,datalen))
                for i in range(atoms):
                    line = fr.readline()
                    data = line.split()
                    atoms_frame[i,:] =  np.array(list(map(float,data)))
                while True:
                    line = fr.readline()
                    if 'END_CFG' in line:
                        break
                frames += 1
                # convert lattice to lammps format
                lammps_lattice = lattice2lammps(lattice_frame)
                lammps_atoms   = atoms_frame[:,2:5]
                lammps_atoms   = np.array(lammps_atoms.dot(np.matrix(lattice_frame).I)).dot(lammps_lattice)
                with open(os.path.join(workpath,dumpname),'a') as fw:
                    fw.write('ITEM: TIMESTEP\n%d\n'%(frames-1))
                    fw.write('ITEM: NUMBER OF ATOMS\n%d\n'%(atoms))
                    fw.write('ITEM: BOX BOUNDS xy xz yz pp pp pp\n')
                    fw.write('%.8e %.8e %.8e\n%.8e %.8e %.8e\n%.8e %.8e %.8e\n'%(lammps_lattice[1,0]+lammps_lattice[2,0],lammps_lattice[0,0],lammps_lattice[1,0],
                                                        lammps_lattice[2,1],lammps_lattice[1,1],lammps_lattice[2,0],0.0,lammps_lattice[2,2],lammps_lattice[2,1]))
                    if not keepforce:
                        fw.write('ITEM: ATOMS id type x y z\n')
                        for i in range(atoms):
                            fw.write('%d %d %.8e %.8e %.8e\n'%(atoms_frame[i,0],atoms_frame[i,1]+1,lammps_atoms[i,0],lammps_atoms[i,1],lammps_atoms[i,2]))
                    elif keepforce:
                        fw.write('ITEM: ATOMS id type x y z fx fy fz\n')
                        for i in range(atoms):
                            fw.write('%d %d %.8e %.8e %.8e %.8e %.8e %.8e\n'%(atoms_frame[i,0],atoms_frame[i,1]+1,lammps_atoms[i,0],lammps_atoms[i,1],lammps_atoms[i,2],
                                                                                  atoms_frame[i,5],atoms_frame[i,6],atoms_frame[i,7]))           
    print("\n%d frames in MLIP's cfg file has been converted to lammps config file...\n"%(frames),flush=True)               
                                                                                 
if __name__ == '__main__':
    inpus= sys.argv[1:]
    path,infile,tofile,rtype = readargv(inpus)
    start= time.perf_counter()
    if rtype == 'lammps':
        cfg2config(path,infile,tofile)
    else:
        cfg2vasp(path,infile)
    end  = time.perf_counter()
    print('Total Time Spent: %.2f s\n'%(end-start),flush=True)